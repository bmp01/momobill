package id.io.momobill.rest.resource;

import id.io.momobill.controller.OtpController;
import id.io.momobill.model.Otp;
import id.io.momobill.rest.model.request.OtpRequest;
import id.io.momobill.validator.OtpValidator;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("otp")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class OtpResource extends BaseResource {

    private OtpController otpController;

    private OtpValidator validator;

    public OtpResource() {
        log = getLogger(this.getClass());
        otpController = new OtpController();
        validator = new OtpValidator();
    }

    @POST
    @Path("validate")
    @Produces(MediaType.APPLICATION_JSON)
    public Response validateOtp(OtpRequest request) {
        final String methodName = "validateOtp";
        Response response = buildBadRequestResponse();
        if (validator.validate(request)) {
            if (otpController.validateOtp(request.getToken())) {
                Otp otp = new Otp();
                otp.setUserId(request.getEmail());
                otp.setToken(request.getToken());
                if (otpController.validateOtp(otp)) {
                    response = buildSuccessResponse();
                } else {
                    response = buildBadRequestResponse("otp_expired");
                }
            } else {
                response = buildBadRequestResponse("invalid_otp");
            }

        }
        completed(methodName);
        return response;
    }

    @POST
    @Path("send")
    @Produces(MediaType.APPLICATION_JSON)
    public Response sendOtp(OtpRequest request) {
        final String methodName = "sendOtp";
        Response response = buildBadRequestResponse();
        if (validator.validateSend(request)) {
            if (otpController.validateUser(request.getEmail())) {
                if (otpController.sendOtp(request.getEmail())) {
                    response = buildSuccessResponse();
                }
            } else {
                response = buildBadRequestResponse("user_not_exist");
            }

        }
        completed(methodName);
        return response;
    }

}
