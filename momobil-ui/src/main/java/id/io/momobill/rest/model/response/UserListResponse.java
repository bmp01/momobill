package id.io.momobill.rest.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.io.momobill.model.Users;

import java.util.List;

public class UserListResponse extends BaseSuccessResponse {

    @JsonProperty("users")
    private List<Users> usersList;

    public UserListResponse(List<Users> usersList) {
        this.usersList = usersList;
    }

    public List<Users> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<Users> usersList) {
        this.usersList = usersList;
    }
}
