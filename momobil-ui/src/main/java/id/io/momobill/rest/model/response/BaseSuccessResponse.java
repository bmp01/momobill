package id.io.momobill.rest.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.io.momobill.rest.util.MessageConstant;

public class BaseSuccessResponse {

    @JsonProperty("status")
    private String status;

    public BaseSuccessResponse() {
        status = MessageConstant.MESSAGE_SUCCESS;
    }

    public BaseSuccessResponse(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
