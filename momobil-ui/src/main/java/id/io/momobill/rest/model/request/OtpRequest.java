package id.io.momobill.rest.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OtpRequest {

    @JsonProperty("email")
    private String email;
    @JsonProperty("token")
    private String token;

    public OtpRequest() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
