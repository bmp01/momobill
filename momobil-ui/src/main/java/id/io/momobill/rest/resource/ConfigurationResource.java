package id.io.momobill.rest.resource;

import id.io.momobill.controller.ConfigurationController;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("configurations")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ConfigurationResource extends BaseResource {

    private ConfigurationController configController;

    public ConfigurationResource() {
        log = getLogger(this.getClass());

        configController = new ConfigurationController();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getConfigurations(@QueryParam("key") String key) {
        final String methodName = "getConfigurations";
        start(methodName);
        Response response = buildBadRequestResponse();
        log.info(methodName, "GET api/configurations?key=" + key);
        if (key.equals("all")) {
            response = buildSuccessResponse(configController.getConfigurations());
        } else if (key.equals("otp")) {
            response = buildSuccessResponse(configController.getOtpConfiguration());
        }
        else if (key.equals("mail")) {
            response = buildSuccessResponse(configController.getMailConfiguration());
        }

        completed(methodName);
        return response;
    }
}
