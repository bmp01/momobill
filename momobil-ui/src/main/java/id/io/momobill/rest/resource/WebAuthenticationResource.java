package id.io.momobill.rest.resource;

import id.io.momobill.controller.AuthenticationController;
import id.io.momobill.controller.ValidationController;
import id.io.momobill.manager.EncryptionManager;
import id.io.momobill.model.Authentication;
import id.io.momobill.model.Users;
import id.io.momobill.rest.model.request.AuthenticationRequest;
import id.io.momobill.rest.model.request.RegistrationRequest;
import id.io.momobill.validator.AuthenticationValidator;

import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

@Path("auth/web/")
@Produces(MediaType.APPLICATION_JSON)
public class WebAuthenticationResource extends BaseResource {

    private AuthenticationController authController;
    private ValidationController validationController;

    private AuthenticationValidator validator;

    public WebAuthenticationResource() {
        log = getLogger(this.getClass());
        authController = new AuthenticationController();
        validationController = new ValidationController();
        validator = new AuthenticationValidator();
    }

    @POST
    @Path("login")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response login(AuthenticationRequest request) {
        final String methodName = "login";
        log.debug(methodName, "POST api/auth/web/login");
        start(methodName);
        Response response = buildBadRequestResponse();
        if (validator.validate(request)) {
            if (validationController.validateEmail(request.getUsername())) {
                String userId = authController.getUserId(request.getUsername());
                Authentication auth = new Authentication();
                auth.setUserId(userId);
                auth.setPassword(EncryptionManager.getInstance().encrypt(request.getPassword()));

                if (authController.authenticate(auth)) {
                    response = buildSuccessResponse();
                } else {
                    response = buildUnauthorizedResponse();
                }
            } else {
                response = buildBadRequestResponse("email_not_exist");
            }
        }
        completed(methodName);
        return response;
    }

    @POST
    @Path("registration")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response registration(RegistrationRequest request) {
        final String methodName = "registration";
        log.debug(methodName, "POST api/auth/web/registration");
        start(methodName);
        Response response = buildBadRequestResponse();
        /*if (validator.validate(request)) {
            String userID = UUID.randomUUID().toString();

            Users users = new Users();
            users.setUserId(userID);
            users.setUserName(request.getEmail());
            users.setFullName(request.getName());
            users.setEmail(request.getEmail());
            users.setPhone(request.getPhone());

            Authentication dbAuthentication = new Authentication();
            dbAuthentication.setUserId(userID);
            dbAuthentication.setPassword(EncryptionManager.getInstance().encrypt(request.getPassword()));

            if (authController..registration(dbAuthentication)) {
                response = buildSuccessResponse();
            } else {
                response = buildUnauthorizedResponse();
            }
        }*/
        completed(methodName);
        return response;
    }

    // unchecked
    @POST
    @Path("logout")
    @PermitAll
    public Response logout() {
        clearSession();
        return buildSuccessResponse();
    }

    @GET
    @Path("session")
    @PermitAll
    public Response session() {
        return buildSuccessResponse();
    }

    private void clearSession() {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }
    }
}
