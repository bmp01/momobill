package id.io.momobill.rest.resource;

import id.io.momobill.controller.MobileController;
import id.io.momobill.controller.ValidationController;
import id.io.momobill.manager.EncryptionManager;
import id.io.momobill.model.Authentication;
import id.io.momobill.model.Registration;
import id.io.momobill.model.Users;
import id.io.momobill.rest.model.request.AuthenticationRequest;
import id.io.momobill.rest.model.request.RegistrationRequest;
import id.io.momobill.rest.model.response.UserResponse;
import id.io.momobill.validator.AuthenticationValidator;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

@Path("auth/mobile")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MobileAuthenticationResource extends BaseResource {

    private MobileController mobileController;

    private AuthenticationValidator authValidator;


    public MobileAuthenticationResource() {
        log = getLogger(this.getClass());
        mobileController = new MobileController();
        authValidator = new AuthenticationValidator();
    }

    @POST
    @Path("register")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response registration(RegistrationRequest request) {
        final String methodName = "registration";
        log.debug(methodName, "POST api/auth/mobile/register");
        start(methodName);
        Response response = buildBadRequestResponse();
        if (authValidator.validate(request)) {
            if (!mobileController.validateEmail(request.getEmail())) {
                Registration registration = new Registration();
                registration.setUserId(UUID.randomUUID().toString());
                registration.setName(request.getName());
                registration.setEmail(request.getEmail());
                registration.setPhone(request.getPhone());
                registration.setPassword(request.getPassword());

                if (mobileController.register(registration)) {
                    response = buildSuccessResponse();
                }
            } else {
                response = buildBadRequestResponse("email_already_in_use");
            }
        }
        completed(methodName);
        return response;
    }

    @POST
    @Path("login")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response login(AuthenticationRequest request) {
        final String methodName = "login";
        log.debug(methodName, "POST api/auth/mobile/login");
        start(methodName);
        Response response = buildBadRequestResponse();
        if (authValidator.validate(request)) {
            if (mobileController.validateEmail(request.getUsername())) {
                String userId = mobileController.getUserId(request.getUsername());
                Authentication auth = new Authentication();
                auth.setUserId(userId);
                auth.setPassword(EncryptionManager.getInstance().encrypt(request.getPassword()));

                if (mobileController.authenticate(auth)) {
                    Users user = mobileController.getUser(userId);
                    UserResponse userResponse = new UserResponse();
                    userResponse.setUserId(user.getUserId());
                    userResponse.setUserName(user.getUserName());
                    userResponse.setFullName(user.getFullName());
                    userResponse.setEmail(user.getEmail());
                    userResponse.setPhone(user.getPhone());
                    userResponse.setStatus(user.isStatus());

                    response = buildSuccessResponse(userResponse);
                } else {
                    response = buildUnauthorizedResponse();
                }
            } else {
                response = buildBadRequestResponse("email_not_exist");
            }
        }
        completed(methodName);
        return response;
    }


}
