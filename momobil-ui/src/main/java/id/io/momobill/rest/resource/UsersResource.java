package id.io.momobill.rest.resource;

import id.io.momobill.rest.model.response.UserListResponse;
import id.io.momobill.controller.UsersController;
import id.io.momobill.util.helper.JsonHelper;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("users")
@Produces(MediaType.APPLICATION_JSON)
public class UsersResource extends BaseResource {

    private final UsersController usersService;

    public UsersResource() {
        log = getLogger(this.getClass());
        usersService = new UsersController();
    }

    @GET
    public Response getUsers() {
        final String methodName = "getUsers";
        log.debug(methodName, "GET api/users");
        start(methodName);

        Response response = buildBadRequestResponse();

        UserListResponse userListResponse = new UserListResponse(usersService.getUsers());

        response = buildSuccessResponse(JsonHelper.toJson(userListResponse));

        completed(methodName);
        return response;
    }
/*
    @GET
    @Path("/{userid}")
    public Response getUser(@PathParam("userid") String userid) {
        final String methodName = "getUser";
        start(methodName);
        log.debug(methodName, "GET api/users/" + userid);

        JSONObject response = new JSONObject();
        try {
            response = usersService.getDetailedUser(userid);
        } catch (JSONException ex) {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_INTERNAL_SERVER_ERROR);
            response.put(ConstantHelper.HTTP_REASON, "error_get_users");
            response.put(ConstantHelper.HTTP_MESSAGE, "Error get users :" + ex.getMessage());
        }
        completed(methodName);
        return Response.ok((!response.has(ConstantHelper.HTTP_CODE))
                ? HttpStatus.SC_OK : response.getInt(ConstantHelper.HTTP_CODE)).entity(response.toString()).build();
    }

    @POST
    @Path("/validate")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response validateUsername(@QueryParam("username") String username) {
        final String methodName = "validateUsername";
        start(methodName);
        log.debug(methodName, "POST api/users/validate");

        JSONObject response = new JSONObject();
        JSONObject json = new JSONObject(username);

        if (usersService.validateUsername(json.getString(ConstantHelper.KEY_USER_USERNAME))) {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_OK);
            response.put(ConstantHelper.HTTP_REASON, "available_username");
            response.put(ConstantHelper.HTTP_MESSAGE, "Username available");
        } else {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_FORBIDDEN);
            response.put(ConstantHelper.HTTP_REASON, "username_in_use");
            response.put(ConstantHelper.HTTP_MESSAGE, "Username already in use");
        }
        completed(methodName);
        return Response.ok((!response.has(ConstantHelper.HTTP_CODE))
                ? HttpStatus.SC_OK : response.getInt(ConstantHelper.HTTP_CODE)).entity(response.toString()).build();
    }

    @PUT
    @Path("/{userid}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateUser(@PathParam("userid") String userid, String jsonPayload) {
        final String methodName = "updateUser";
        start(methodName);
        log.debug(methodName, "PUT api/users");

        JSONObject response = new JSONObject();
        JSONObject json = new JSONObject(jsonPayload);

        if (userid.equals(json.getString(ConstantHelper.KEY_USER_USERID))) {
            if (usersService.updateUser(json)) {
                response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_OK);
                response.put(ConstantHelper.HTTP_REASON, "update_user_successful");
                response.put(ConstantHelper.HTTP_MESSAGE, "Successful update user " + userid);
            } else {
                response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_BAD_REQUEST);
                response.put(ConstantHelper.HTTP_REASON, "error_update_user");
                response.put(ConstantHelper.HTTP_MESSAGE, "Error update user");
            }

        } else {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_FORBIDDEN);
            response.put(ConstantHelper.HTTP_REASON, "invalid_users");
            response.put(ConstantHelper.HTTP_MESSAGE, "Invalid users :");
        }
        completed(methodName);
        return Response.ok((!response.has(ConstantHelper.HTTP_CODE))
                ? HttpStatus.SC_OK : response.getInt(ConstantHelper.HTTP_CODE)).entity(response.toString()).build();

    }*/
}
