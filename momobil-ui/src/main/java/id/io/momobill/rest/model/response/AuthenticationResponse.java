package id.io.momobill.rest.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.io.momobill.model.Users;

public class AuthenticationResponse extends BaseSuccessResponse {

    @JsonProperty("result")
    private Users usersResponse;

    public AuthenticationResponse(Users usersResponse) {
        this.usersResponse = usersResponse;
    }

    public Users getUsersResponse() {
        return usersResponse;
    }

    public void setUsersResponse(Users usersResponse) {
        this.usersResponse = usersResponse;
    }
}
