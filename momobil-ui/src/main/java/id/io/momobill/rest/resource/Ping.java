/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.momobill.rest.resource;

import id.io.momobill.controller.ConfigurationController;
import id.io.momobill.helper.MailHelper;
import id.io.momobill.helper.OTPGeneratorHelper;
import id.io.momobill.manager.EncryptionManager;
import id.io.momobill.model.Otp;
import id.io.momobill.util.constant.ConstantHelper;
import id.io.momobill.util.database.OtpDatabaseHelper;
import id.io.momobill.util.helper.JsonHelper;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;

@Path("ping")
public class Ping extends BaseResource {

    private ConfigurationController configController;
    private OtpDatabaseHelper otpController;

    public Ping() {
        log = getLogger(this.getClass());
        configController = new ConfigurationController();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPingTest() {
        final String methodName = "getPingTest";
        start(methodName);
        log.info(methodName, "GET /ping");
        Response response = buildSuccessResponse();
        completed(methodName);
        return response;
    }

    @POST
    @Path("/mail")
    @Produces(MediaType.APPLICATION_JSON)
    public Response dumnySendMail() {
        final String methodName = "dumnySendMail";
        log.debug(methodName, "POST /mail");
        start(methodName);

        HashMap<String, String> mailConfig = configController.getMailConfiguration();
        HashMap<String, String> otpConfig = configController.getOtpConfiguration();

        log.debug("mailPass", EncryptionManager.getInstance().decrypt(mailConfig.get(ConstantHelper.MAIL_PASSWORD)));

        String recipient = "bachtiar.madya.p@gmail.com";
        log.debug("recipient", JsonHelper.toJson(recipient));

        String token = OTPGeneratorHelper.generateOtp(Integer.parseInt(otpConfig.get(ConstantHelper.OTP_LENGTH)));
        log.debug("token", JsonHelper.toJson(token));

        String subject = otpConfig.get(ConstantHelper.OTP_MAIL_SUBJECT);
        log.debug("subject", JsonHelper.toJson(subject));

        String content = otpConfig.get(ConstantHelper.OTP_MAIL_CONTENT);
        log.debug("content", JsonHelper.toJson(content));


        MailHelper mailHelper = new MailHelper();
        mailHelper.sendMail(recipient, content, subject);

        completed(methodName);
        return Response.ok().build();
    }
}
