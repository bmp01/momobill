package id.io.momobill.validator;

import id.io.momobill.rest.model.request.AuthenticationRequest;
import id.io.momobill.rest.model.request.RegistrationRequest;

public class AuthenticationValidator extends BaseValidator {

    public boolean validate(AuthenticationRequest request) {
        return notNull(request) && validate(request.getUsername()) && validate(request.getPassword());
    }

    public boolean validate(RegistrationRequest request) {
        return notNull(request) && validate(request.getEmail()) && validate(request.getPassword())
                && validate(request.getName()) && validate(request.getPhone());
    }

}
