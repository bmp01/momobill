package id.io.momobill.validator;

import id.io.momobill.rest.model.request.OtpRequest;

public class OtpValidator extends BaseValidator {

    public boolean validate(OtpRequest request) {
        return notNull(request) && validate(request.getEmail()) && validate(request.getToken());
    }

    public boolean validateSend(OtpRequest request) {
        return notNull(request) && validate(request.getEmail());
    }
}
