/*
 * /**
 *   * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *   *
 *   * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 *   * as indicated by the @author tags. All Rights Reserved
 *   *
 *   * The contents of this file are subject to the terms of the
 *   * Common Development and Distribution License (the License).
 *   *
 *   * Everyone is permitted to copy and distribute verbatim copies
 *   * of this license document, but changing it is not allowed.
 *   *
 */
package id.io.momobill.util.constant;

public class ConstantHelper {
    
    public static final String CONFIG_FILE                  = "momobill.properties";

    // Session Configuration
    public static final String SESSION_KEY                  = "momobill-session-key";

    // REST Endpoints
    public static final String HTTP_STATUS_CODE             = "statusCode";
    public static final String HTTP_RESPONSE                = "response";
    public static final String HTTP_REASON                  = "reason";
    public static final String HTTP_CODE                    = "code";
    public static final String HTTP_MESSAGE                 = "message";
    public static final String HTTP_SUCCESSFUL              = "successful";

    // User Key Configurations
    public static final String KEY_USER_USERID              = "user-id";
    public static final String KEY_USER_PASSWORD            = "password";
    public static final String KEY_USER_USERNAME            = "user-name";
    public static final String KEY_USER_FULLNAME            = "full-name";
    public static final String KEY_USER_EMAIL               = "email";
    public static final String KEY_USER_PHONE               = "phone";
    public static final String KEY_USER_AVATAR              = "pictures";
    public static final String KEY_USER_ADDRESSES           = "addresses";
    public static final String KEY_USER_CREATEDT            = "create-dt";

    public static final String KEY_ADDRESS_TAGS             = "tags";
    public static final String KEY_ADDRESS_ADDRESS          = "address";
    public static final String KEY_ADDRESS_LATITUDE         = "latitude";
    public static final String KEY_ADDRESS_LONGITUDE        = "longitude";



    //Error Reason Configurations
    public static final String ERROR_REASON_UNAUTHORIZED    = "unauthorized";
    public static final String ERROR_REASON_USER_NOT_FOUND  = "user_not_found";


    //Error Message Configurations
    public static final String ERROR_MESSAGE_UNAUTHORIZED    = "Unauthorized";
    public static final String ERROR_MESSAGE_USER_NOT_FOUND  = "User Not Found!";

    //OTP CONFIG
    public static final String OTP_ENABLED                  = "OTP_ENABLED";
    public static final String OTP_VALIDITY                 = "OTP_VALIDITY";
    public static final String OTP_LENGTH                   = "OTP_LENGTH";
    public static final String OTP_MAIL_SUBJECT             = "OTP_MAIL_SUBJECT";
    public static final String OTP_MAIL_CONTENT             = "OTP_MAIL_CONTENT";

    //MAIL CONFIG
    public static final String MAIL_HOST_NAME                   = "MAIL_HOST_NAME";
    public static final String MAIL_SMTP_PORT                   = "MAIL_SMTP_PORT";
    public static final String MAIL_USERNAME                    = "MAIL_USERNAME";
    public static final String MAIL_PASSWORD                    = "MAIL_PASSWORD";
    public static final String MAIL_SECURED_SMTP                = "MAIL_SECURED_SMTP";
    public static final String MAIL_SMTP_SSL                    = "MAIL_SMTP_SSL";
    public static final String MAIL_SMTP_AUTH                   = "MAIL_SMTP_AUTH";
    public static final String MAIL_SMTP_TLS                    = "MAIL_SMTP_TLS";
    public static final String MAIL_SERVER_FROM_ADDRESS         = "MAIL_SERVER_FROM_ADDRESS";
    public static final String MAIL_SERVER_PREFIX               = "MAIL_SERVER_PREFIX";
    public static final String MAIL_SMTP_SOCKETFACTORY_CLASS    = "MAIL_SMTP_SOCKETFACTORY_CLASS";
}
