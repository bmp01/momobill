package id.io.momobill.util.database;

import id.io.momobill.model.Authentication;
import id.io.momobill.util.constant.SqlConstant;
import org.jdbi.v3.core.Handle;

import java.sql.SQLException;

public class AuthenticationDatabaseHelper extends BaseDatabaseHelper {

    public AuthenticationDatabaseHelper() {
        log = getLogger(this.getClass());
    }

    public boolean createAuthentication(Authentication data) {
        String methodName = "createAuthentication";
        start(methodName);
        boolean result = false;

        final String sql = SqlConstant.PG_AUTHENTICATION_CREATE_CLAUSE;
        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(sql)
                    .bind(SqlConstant.SQL_PARAM_USERID, data.getUserId())
                    .bind(SqlConstant.SQL_PARAM_PASSWORD, data.getPassword())
                    .execute();
            if (row == 1) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorCreateAuthentication");
        }
        completed(methodName);
        return result;
    }

    public boolean updatePassword(Authentication data) {
        String methodName = "updatePassword";
        start(methodName);
        boolean result = false;

        final String sql = SqlConstant.PG_AUTHENTICATION_UPDATE_PASSWORD_CLAUSE;
        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(sql)
                    .bind(SqlConstant.SQL_PARAM_USERID, data.getUserId())
                    .bind(SqlConstant.SQL_PARAM_PASSWORD, data.getPassword())
                    .execute();
            if (row == 1) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorUpdatePassword");
        }
        completed(methodName);
        return result;
    }

    public boolean authenticate(Authentication data) {
        String methodName = "authenticate";
        start(methodName);
        boolean result = false;

        final String sql = SqlConstant.PG_AUTHENTICATION_AUTHENTICATE_CLAUSE;
        try (Handle handle = getHandle()) {
            int row = handle.createQuery(sql)
                    .bind(SqlConstant.SQL_PARAM_USERID, data.getUserId())
                    .bind(SqlConstant.SQL_PARAM_PASSWORD, data.getPassword())
                    .mapTo(Integer.class).first();
            if (row == 1) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorAuthenticate");
        }
        completed(methodName);
        return result;
    }

    public boolean updateUserLastLoginTimestamp(Authentication data) {
        String methodName = "updateUserLastLoginTimestamp";
        start(methodName);

        boolean result = false;
        final String sql = SqlConstant.PG_AUTHENTICATION_UPDATE_LOGIN_TIME_CLAUSE;
        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(sql).bind(SqlConstant.SQL_PARAM_USERID, data.getUserId())
                    .execute();
            if (row == 1) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorUpdateUserLastLoginTimestamp");
        }
        completed(methodName);
        return result;
    }

    public boolean deleteAuthentication(String userId) {
        String methodName = "deleteAuthentication";
        start(methodName);
        boolean result = false;

        final String sql = SqlConstant.PG_AUTHENTICATION_DELETE_CLAUSE;
        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(sql).bind(SqlConstant.SQL_PARAM_USERID, userId)
                    .execute();
            if (row == 1) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorDeleteAuthentication");
        }
        completed(methodName);
        return result;
    }
}
