/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 *
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 *
 */
package id.io.momobill.util.constant;

public class ApplicationConstant {

    /**
     * The Constant BLANK.
     */
    public static final String BLANK = "";

    /**
     * The Constant COLON.
     */
    public static final String COLON = " : ";

    /**
     * The Constant DASH.
     */
    public static final String DASH = " - ";

    /**
     * The Constant KEY_TXNID.
     */
    public static final String KEY_TXNID = "TXNID";

    /**
     * The Constant XPATH_TXNID.
     */
    public static final String XPATH_TXNID = "//transcationId";

    /**
     * The Constant ENTRY.
     */
    public static final String ENTRY = "Entry";

    /**
     * The Constant EXIT.
     */
    public static final String EXIT = "Exit";

}
