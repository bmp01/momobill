package id.io.momobill.util.database;

import id.io.momobill.model.Configuration;
import id.io.momobill.util.constant.SqlConstant;
import org.jdbi.v3.core.Handle;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ConfigurationDatabaseHelper extends BaseDatabaseHelper {

    public ConfigurationDatabaseHelper() {
        log = getLogger(this.getClass());
    }

    public HashMap<String, String> getConfiguration() {
        String methodName = "getConfiguration";
        HashMap<String, String> result = new HashMap<>();
        List<Configuration> configList = new ArrayList<>();
        start(methodName);

        final String sql = SqlConstant.PG_CONFIGURATION_GET_CLAUSE;
        try (Handle handle = getHandle()) {
            configList = handle.createQuery(sql).mapToBean(Configuration.class).list();
            for (Configuration configuration : configList) {
                result.put(configuration.getKey(), configuration.getValue());
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorGetConfiguration");
        }

        completed(methodName);
        return result;
    }

    public HashMap<String, String> getOtpConfiguration() {
        String methodName = "getOtpConfiguration";
        HashMap<String, String> result = new HashMap<>();
        List<Configuration> configList = new ArrayList<>();
        start(methodName);

        final String sql = SqlConstant.PG_CONFIGURATION_GET_CLAUSE + " WHERE config_key LIKE 'OTP_%';";
        try (Handle handle = getHandle()) {
            configList = handle.createQuery(sql).mapToBean(Configuration.class).list();
            for (Configuration configuration : configList) {
                result.put(configuration.getKey(), configuration.getValue());
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorGetOtpConfiguration");
        }

        completed(methodName);
        return result;
    }

    public HashMap<String, String> getMailConfiguration() {
        String methodName = "getMailConfiguration";
        HashMap<String, String> result = new HashMap<>();
        List<Configuration> configList = new ArrayList<>();
        start(methodName);

        final String sql = SqlConstant.PG_CONFIGURATION_GET_CLAUSE + " WHERE config_key LIKE 'MAIL_%';";
        try (Handle handle = getHandle()) {
            configList = handle.createQuery(sql).mapToBean(Configuration.class).list();
            for (Configuration configuration : configList) {
                result.put(configuration.getKey(), configuration.getValue());
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorGetMailConfiguration");
        }

        completed(methodName);
        return result;
    }
}
