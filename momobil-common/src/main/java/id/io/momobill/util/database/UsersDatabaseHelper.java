/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.momobill.util.database;

import id.io.momobill.model.Users;
import id.io.momobill.util.constant.SqlConstant;
import org.jdbi.v3.core.Handle;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UsersDatabaseHelper extends BaseDatabaseHelper {

    public UsersDatabaseHelper() {
        log = getLogger(this.getClass());
    }

    public String getUserId(String email) {
        String methodName = "getUserId";
        start(methodName);
        String result = null;
        final String sql = SqlConstant.PG_USER_GET_USERID_BY_EMAIL_CLAUSE;
        try (Handle handle = getHandle()) {
            result = handle.createQuery(sql)
                    .bind(SqlConstant.SQL_PARAM_EMAIL, email).mapTo(String.class).first();
        } catch (SQLException ex) {
            log.error(methodName, "errorGetUserId");
        }
        completed(methodName);
        return result;
    }

    public String getEmail(String userid) {
        String methodName = "getEmail";
        String result = null;
        start(methodName);

        final String sql = SqlConstant.PG_USER_GET_EMAIL_CLAUSE;

        try (Handle handle = getHandle()) {
            result = handle.createQuery(sql)
                    .bind(SqlConstant.SQL_PARAM_USERID, userid).mapTo(String.class).first();
        } catch (SQLException ex) {
            log.error(methodName, "errorGetEmail");
        }

        completed(methodName);
        return result;
    }

    public List<Users> getUsers() {
        String methodName = "getUsers";
        List<Users> usersList = new ArrayList<>();
        start(methodName);

        final String sql = SqlConstant.PG_USER_GET_USER_LIST_CLAUSE;
        try (Handle handle = getHandle()) {
            usersList = handle.createQuery(sql)
                    .mapToBean(Users.class).list();
        } catch (SQLException ex) {
            log.error(methodName, "errorGetUsers");
        }

        completed(methodName);
        return usersList;
    }

    public Users getUser(String userId) {
        String methodName = "getUser";
        Users user = new Users();
        start(methodName);

        final String sql = SqlConstant.PG_USER_GET_USER_CLAUSE;
        try (Handle handle = getHandle()) {
            user = handle.createQuery(sql)
                    .bind(SqlConstant.SQL_PARAM_USERID, userId)
                    .mapToBean(Users.class).first();
        } catch (SQLException ex) {
            log.error(methodName, "errorGetUser");
        }

        completed(methodName);
        return user;
    }

    public boolean validateUsername(String username) {
        String methodName = "validateUsername";
        start(methodName);

        boolean result = false;
        final String sql = SqlConstant.PG_USER_VALIDATE_USERNAME_CLAUSE;
        try (Handle handle = getHandle()) {
            int row = handle.createQuery(sql)
                    .bind(SqlConstant.SQL_PARAM_USERNAME, username)
                    .mapTo(Integer.class).first();
            if (row == 1) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorValidateUsername");
        }
        completed(methodName);
        return result;
    }

    public boolean validateEmail(String email) {
        String methodName = "validateEmail";
        start(methodName);

        boolean result = false;
        final String sql = SqlConstant.PG_USER_VALIDATE_EMAIL_CLAUSE;
        try (Handle handle = getHandle()) {
            int row = handle.createQuery(sql)
                    .bind(SqlConstant.SQL_PARAM_EMAIL, email)
                    .mapTo(Integer.class).first();
            if (row == 1) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorValidateUsername");
        }
        completed(methodName);
        return result;
    }

    public boolean addUser(Users users) {
        String methodName = "addUser";
        start(methodName);

        boolean result = false;
        final String sql = SqlConstant.PG_USER_INSERT_CLAUSE;
        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(sql).bindBean(users).execute();
            if (row == 1) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorAddUser");
        }
        completed(methodName);
        return result;
    }

    public boolean updateUser(Users users) {
        String methodName = "updateUser";
        start(methodName);

        boolean result = false;
        final String sql = SqlConstant.PG_USER_UPDATE_CLAUSE;
        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(sql).bindBean(users).execute();
            if (row == 1) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorUpdateUser");
        }
        completed(methodName);
        return result;
    }

    public boolean activateUser(String userId) {
        String methodName = "activateUser";
        start(methodName);

        boolean result = false;
        final String sql = SqlConstant.PG_USER_ACTIVATE_CLAUSE;
        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(sql).bind(SqlConstant.SQL_PARAM_USERID, userId).execute();
            if (row == 1) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorActivateUser");
        }
        completed(methodName);
        return result;
    }

    public boolean deleteUser(String userId) {
        String methodName = "deleteUser";
        start(methodName);

        boolean result = false;
        final String sql = SqlConstant.PG_USER_DELETE_CLAUSE;
        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(sql)
                    .bind(SqlConstant.SQL_PARAM_USERID, userId).execute();
            if (row == 1) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorDeleteUser");
        }
        completed(methodName);
        return result;
    }
}
