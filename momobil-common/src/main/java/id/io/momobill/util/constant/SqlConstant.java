package id.io.momobill.util.constant;

public class SqlConstant {

    //Global parameter
    public static final String SQL_PARAM_USERNAME = "userName";
    public static final String SQL_PARAM_PASSWORD = "password";
    public static final String SQL_PARAM_USERID = "userId";
    public static final String SQL_PARAM_FULLNAME = "fullName";
    public static final String SQL_PARAM_EMAIL = "email";
    public static final String SQL_PARAM_PHONE = "phone";
    public static final String SQL_PARAM_PICTURE = "picture";
    public static final String SQL_PARAM_ROLE_ID = "roleId";
    public static final String SQL_PARAM_CREATE_DT = "createDt";


    //OTP Param
    public static final String SQL_PARAM_TOKEN = "token";

    //PG Configuration
    public static final String PG_ALL_CONFIGURATION_GET_CLAUSE = "SELECT config_key AS key, config_value AS value FROM configurations;";
    public static final String PG_CONFIGURATION_GET_CLAUSE = "SELECT config_key AS key, config_value AS value FROM configurations";

    //PG Authentication
    public static final String PG_AUTHENTICATION_CREATE_CLAUSE = "INSERT INTO authentication (user_id, \"password\") VALUES(:userId, :password);";
    public static final String PG_AUTHENTICATION_UPDATE_PASSWORD_CLAUSE = "UPDATE authentication SET \"password\" = :password WHERE user_id= :userId;";
    public static final String PG_AUTHENTICATION_AUTHENTICATE_CLAUSE = "SELECT COUNT(1) FROM authentication WHERE user_id = :userId AND \"password\" = :password;";
    public static final String PG_AUTHENTICATION_UPDATE_LOGIN_TIME_CLAUSE = "UPDATE authentication SET last_login=now() WHERE user_id = :userId;";
    public static final String PG_AUTHENTICATION_DELETE_CLAUSE = "DELETE FROM authentication WHERE user_id = :userId;";

    //PG Admin
    public static final String PG_ADMIN_GET_USERID_CLAUSE = "SELECT user_id AS userId FROM admin WHERE username =:username;";
    public static final String PG_ADMIN_VALIDATE_USERID_CLAUSE = "SELECT count(0) FROM admin where user_id = :userId;";
    public static final String PG_ADMIN_GET_LIST_CLAUSE = "SELECT user_id AS userId, username AS username, fullname AS fullname, email AS email, phone AS phone, roles AS roleId, create_dt AS createDt, last_update AS lastUpdate FROM admin;";
    public static final String PG_ADMIN_GET_CLAUSE = "SELECT user_id AS userId, username AS username, fullname AS fullname, email AS email, phone AS phone, roles AS roleId, create_dt AS createDt, last_update AS lastUpdate FROM admin WHERE user_id = :userId;";
    public static final String PG_ADMIN_INSERT_CLAUSE = "INSERT INTO admin (user_id, username, fullname, email, phone, roles, create_dt, last_update) VALUES(:userId, :username, :fullname, :email, :phone, :roles, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);";
    public static final String PG_ADMIN_UPDATE_CLAUSE = "UPDATE admin SET  username= :username, fullname= :fullname, email= :email, phone= :phone, roles= :roleId, last_update=CURRENT_TIMESTAMP WHERE user_id= :userId;";
    public static final String PG_ADMIN_DELETE_CLAUSE = "DELETE FROM admin WHERE user_id= :userId;";

    //PG User
    public static final String PG_USER_GET_USERID_BY_EMAIL_CLAUSE = "SELECT user_id AS userId FROM users WHERE email = :email;";
    public static final String PG_USER_GET_EMAIL_CLAUSE = "SELECT email from users WHERE user_id = :userid;";
    public static final String PG_USER_GET_USER_LIST_CLAUSE = "SELECT user_id AS userId, username AS userName, fullname AS fullName, email AS email, phone AS phone, pict AS picture, create_dt AS createDt FROM users;";
    public static final String PG_USER_GET_USER_CLAUSE = "SELECT user_id AS userId, username AS userName, fullname AS fullName, email AS email, phone AS phone, status AS status FROM users WHERE user_id = :userId;";
    public static final String PG_USER_VALIDATE_USERNAME_CLAUSE = "SELECT COUNT(1) FROM users WHERE username = :username;";
    public static final String PG_USER_VALIDATE_EMAIL_CLAUSE = "SELECT COUNT(1) FROM users WHERE email = :email;";
    public static final String PG_USER_INSERT_CLAUSE = "INSERT INTO users (user_id, username, fullname, email, phone, pict, create_dt) VALUES(:userId, :userName, :fullName, :email, :phone, :picture, CURRENT_TIMESTAMP);";
    public static final String PG_USER_UPDATE_CLAUSE = "UPDATE users SET username= :userName, fullname= :fullName, email= :email, phone= :phone WHERE user_id= :userid;";
    public static final String PG_USER_ACTIVATE_CLAUSE = "UPDATE users SET status= true WHERE user_id= :userId;";
    public static final String PG_USER_DELETE_CLAUSE = "DELETE FROM users WHERE user_id=:userId;";

    //PG OTP
    public static final String PG_OTP_INSERT_CLAUSE = "INSERT INTO otp (user_id, \"token\", create_dt) VALUES(:userId, :token, CURRENT_TIMESTAMP);";
    public static final String PG_OTP_VALIDATE_TOKEN_CLAUSE ="SELECT COUNT(1) FROM otp WHERE token =:token;";
    public static final String PG_OTP_VALIDATE_CLAUSE = "SELECT COUNT(1) FROM otp WHERE user_id = :userId AND token = :token AND create_dt > :createDt";
    public static final String PG_OTP_DELETE_CLAUSE = "DELETE FROM otp WHERE user_id= :userId;";



}
