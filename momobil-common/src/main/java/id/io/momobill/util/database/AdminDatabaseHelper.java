package id.io.momobill.util.database;

import id.io.momobill.model.DBAdmin;
import id.io.momobill.util.constant.SqlConstant;
import org.jdbi.v3.core.Handle;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AdminDatabaseHelper extends BaseDatabaseHelper {

    public AdminDatabaseHelper() {
        log = getLogger(this.getClass());
    }

    public String getUserId(String username) {
        String methodName = "getUserId";
        start(methodName);
        String result = null;
        final String sql = SqlConstant.PG_ADMIN_GET_USERID_CLAUSE;
        try (Handle handle = getHandle()) {
            result = handle.createQuery(sql)
                    .bind(SqlConstant.SQL_PARAM_USERNAME, username).mapTo(String.class).first();
        } catch (SQLException ex) {
            log.error(methodName, "errorGetUserId");
        }
        completed(methodName);
        return result;
    }

    public List<DBAdmin> getAdmins() {
        String methodName = "getAdmin";
        List<DBAdmin> list = new ArrayList<>();
        start(methodName);

        final String sql = SqlConstant.PG_ADMIN_GET_LIST_CLAUSE;
        try (Handle handle = getHandle()) {
            list = handle.createQuery(sql)
                    .mapToBean(DBAdmin.class).list();
        } catch (SQLException ex) {
            log.error(methodName, "errorGetAdmin");
        }
        completed(methodName);
        return list;
    }

    public DBAdmin getAdmin(String userid) {
        String methodName = "getAdmin";
        DBAdmin admin = new DBAdmin();
        start(methodName);

        final String sql = SqlConstant.PG_ADMIN_GET_CLAUSE;
        try (Handle handle = getHandle()) {
            admin = handle.createQuery(sql)
                    .bind(SqlConstant.SQL_PARAM_USERID, userid)
                    .mapToBean(DBAdmin.class).first();
        } catch (SQLException ex) {
            log.error(methodName, "errorGetAdmin");
        }

        completed(methodName);
        return admin;
    }

    public boolean createAdmin(DBAdmin adminDb) {
        String methodName = "createAdmin";
        start(methodName);
        boolean result = false;

        final String sql = SqlConstant.PG_ADMIN_INSERT_CLAUSE;
        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(sql).bindBean(adminDb)
                    .execute();
            if (row == 1) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorCreateAdmin");
        }
        completed(methodName);
        return result;
    }

    public boolean updateAdmin(DBAdmin adminDb) {
        String methodName = "updateAdmin";
        start(methodName);
        boolean result = false;

        final String sql = SqlConstant.PG_ADMIN_UPDATE_CLAUSE;
        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(sql).bindBean(adminDb)
                    .execute();
            if (row == 1) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorUpdateAdmin");
        }
        completed(methodName);
        return result;
    }

    public boolean validateAdmin(String userId) {
        String methodName = "validateAdmin";
        start(methodName);
        boolean result = false;

        final String sql = SqlConstant.PG_ADMIN_VALIDATE_USERID_CLAUSE;
        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(sql).bind(SqlConstant.SQL_PARAM_USERID, userId)
                    .execute();
            if (row == 1) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorValidateAdmin");
        }
        completed(methodName);
        return result;
    }

    public boolean deleteAdmin(String userId) {
        String methodName = "deleteAdmin";
        start(methodName);
        boolean result = false;

        final String sql = SqlConstant.PG_ADMIN_DELETE_CLAUSE;
        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(sql).bind(SqlConstant.SQL_PARAM_USERID, userId)
                    .execute();
            if (row == 1) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorDeleteAdmin");
        }
        completed(methodName);
        return result;
    }
}
