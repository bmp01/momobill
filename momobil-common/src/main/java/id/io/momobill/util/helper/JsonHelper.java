/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2019 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.momobill.util.helper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.io.momobill.util.helper.json.CustomObjectMapper;
import id.io.momobill.util.log.BaseLogger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JsonHelper {

    private static final BaseLogger log = new BaseLogger(JsonHelper.class);
    private static final ObjectMapper OBJ_MAPPER = CustomObjectMapper.getInstance().getObjectMapper();

    private JsonHelper() {
        // Empty Constructor
    }

    public static String toJson(Object obj) {
        try {
            return OBJ_MAPPER.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            log.error("toJson", e);
        }
        return "";
    }

    public static <T> T fromJson(String json, Class<T> clazz) {
        try {
            return OBJ_MAPPER.readValue(json, clazz);
        } catch (IOException e) {
            log.error("fromJson", e);
        }

        try {
            return clazz.getConstructor().newInstance();
        } catch (Exception ex) {
            log.error("fromJson", "Could not invoke Default Constructor", ex);
        }

        return null;
    }

    public static <T> List<T> fromJsonArray(String json) {
        try {
            return OBJ_MAPPER.readValue(json, new TypeReference<List<T>>() {
            });
        } catch (IOException e) {
            log.error("fromJsonArray", e);
        }
        return new ArrayList<>();
    }
}
