package id.io.momobill.util.database;

import id.io.momobill.model.Otp;
import id.io.momobill.util.constant.SqlConstant;
import org.jdbi.v3.core.Handle;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;

public class OtpDatabaseHelper extends BaseDatabaseHelper {

    public OtpDatabaseHelper() {
        log = getLogger(this.getClass());
    }

    public boolean saveOtpToken(Otp otp) {
        String methodName = "saveOtpToken";
        start(methodName);

        boolean result = false;
        final String sql = SqlConstant.PG_OTP_INSERT_CLAUSE;
        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(sql).bindBean(otp).execute();
            if (row == 1) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorSaveOtpToken");
        }
        completed(methodName);
        return result;

    }

    public boolean validateOtp(String token) {
        final String methodName = "validateOtp";
        start(methodName);

        boolean result = false;

        final String sql = SqlConstant.PG_OTP_VALIDATE_TOKEN_CLAUSE;

        try (Handle h = getHandle()) {

            int count = h.createQuery(sql).bind(SqlConstant.SQL_PARAM_TOKEN, token)
                    .mapTo(Integer.class).findOnly();
            if (count > 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorValidateOtp");
        }
        completed(methodName);
        return result;
    }

    public boolean validateOtp(String userId, String token, int expiry) {
        final String methodName = "validateOtp";
        start(methodName);

        boolean result = false;

        final String sql = SqlConstant.PG_OTP_VALIDATE_CLAUSE;

        try (Handle h = getHandle()) {
            LocalDateTime createDt = LocalDateTime.now().minusMinutes(expiry);
            Timestamp expiredDt = Timestamp.valueOf(createDt);
            int count = h.createQuery(sql)
                    .bind(SqlConstant.SQL_PARAM_USERID, userId)
                    .bind(SqlConstant.SQL_PARAM_TOKEN, token)
                    .bind(SqlConstant.SQL_PARAM_CREATE_DT, expiredDt)
                    .mapTo(Integer.class).findOnly();
            if (count != 0) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorValidateOTP");
        }
        completed(methodName);
        return result;
    }

    public boolean deleteOtpToken(String userId) {
        String methodName = "deleteOtpToken";
        start(methodName);

        boolean result = false;
        final String sql = SqlConstant.PG_OTP_DELETE_CLAUSE;
        try (Handle handle = getHandle()) {
            int row = handle.createUpdate(sql).bind(SqlConstant.SQL_PARAM_USERID, userId).execute();
            if (row == 1) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, "errorDeleteOtpToken");
        }
        completed(methodName);
        return result;

    }
}
