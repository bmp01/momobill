package id.io.momobill.util.database.refactor;

import id.io.momobill.model.DBUsersAddress;
import id.io.momobill.util.database.BaseDatabaseHelper;
import org.jdbi.v3.core.Handle;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UsersAddressDatabaseHelper extends BaseDatabaseHelper {

    public UsersAddressDatabaseHelper() {
        log = getLogger(this.getClass());
    }

    public List<DBUsersAddress> getAddresses(String userId) {
        String methodName = "getAddresses";
        List<DBUsersAddress> usersAddress = new ArrayList<>();
        start(methodName);

        final String sql = "SELECT tags, address, latitude, longitude FROM users_address WHERE userid = :userid;";
        try (Handle handle = getHandle()) {
            usersAddress = handle.createQuery(sql)
                    .bind("userid", userId)
                    .mapToBean(DBUsersAddress.class).list();
        } catch (SQLException ex) {
            log.error(methodName, " - errorGetAddresses : " + ex);
        }
        completed(methodName);
        return usersAddress;
    }

    public boolean updateAddress(DBUsersAddress address, String oldTags) {
        String methodName = "updateAddress";
        Boolean result = false;
        start(methodName);

        final String sql = "UPDATE users_address SET tags= :tags, address= :address, latitude= :latitude, longitude= :longitude WHERE userid= :userid AND tags= :oldTags;";
        int row = 0;
        try (Handle handle = getHandle()) {
            row = handle.createUpdate(sql).bindBean(address).bind("oldTags", oldTags)
                    .execute();
            if (row == 1) {
                result = true;
            }
        } catch (SQLException ex) {
            log.error(methodName, " - updateAddress : " + ex);
        }
        completed(methodName);
        return result;
    }
}
