package id.io.momobill.model;

public class DBUsersAddress {
    private String userid;
    private String address;
    private String tags;
    private String latitude;
    private String longitude;

    public DBUsersAddress() {
    }

    public DBUsersAddress(String userid, String address, String tags, String latitude, String longitude) {
        this.userid = userid;
        this.address = address;
        this.tags = tags;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
