package id.io.momobill.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Authentication {

    @JsonProperty("user_id")
    private String userId;
    @JsonProperty("password")
    private String password;

    public Authentication() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
