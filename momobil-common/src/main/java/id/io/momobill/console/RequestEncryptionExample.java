package id.io.momobill.console;

import id.io.momobill.manager.EncryptionManager;

public class RequestEncryptionExample {

    public static void main(String[] args)
    {
        String request = "{\n" +
                "    \"email\": \"test\",\n" +
                "    \"password\": \"test\",\n" +
                "    \"name\": \"test\",\n" +
                "    \"phone\": \"test\"\n" +
                "}";

        String encRequest = "3EntG6/OBH4hEZ9bBt7EFaykwzfTgmBby9jgh68YpPnm2iTk0BiSnFmFPiWAdLFQgSwHoUp2a/lOLb2H7k78KJGQdEtMXZOnSeuQ0z6zQvbfQKHny2o0AC/6LSclBPYv";

        System.out.println(EncryptionManager.getInstance().encrypt(request));
        System.out.println(EncryptionManager.getInstance().decrypt(encRequest));
    }
}
