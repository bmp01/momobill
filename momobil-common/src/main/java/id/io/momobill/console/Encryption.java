/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * <p>
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 * <p>
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * <p>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */
package id.io.momobill.console;

import id.io.momobill.manager.EncryptionManager;

public class Encryption {

    public static void main(String[] args) {
        System.out.println(EncryptionManager.getInstance().encrypt("Secret123!"));
        System.out.println(EncryptionManager.getInstance().decrypt("AquMy/6jndK+9GlqHb9HVw=="));
    }

}
