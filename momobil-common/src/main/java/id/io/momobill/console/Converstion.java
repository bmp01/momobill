/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 *
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 *
 */
package id.io.momobill.console;

import java.util.concurrent.TimeUnit;

/**
 *
 * @author bmp
 */
public class Converstion {

    public static void main(String[] args) {
        String seconds = "3600";
        String minute = "60";

        System.out.println(convertSecondToMinute(seconds));
        System.out.println(convertMinuteToSecond(minute));
    }

    public static String convertSecondToMinute(String value) {
        int unit = Integer.parseInt(value);
        return String.valueOf(TimeUnit.SECONDS.toMinutes(unit));
    }
    public static String convertMinuteToSecond(String value) {
        int unit = Integer.parseInt(value);
        return String.valueOf(TimeUnit.MINUTES.toSeconds(unit));
    }

}
