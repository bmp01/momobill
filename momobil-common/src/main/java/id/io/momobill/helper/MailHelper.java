package id.io.momobill.helper;

import id.io.momobill.manager.EncryptionManager;
import id.io.momobill.util.constant.ConstantHelper;
import id.io.momobill.util.database.ConfigurationDatabaseHelper;
import id.io.momobill.util.helper.JsonHelper;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.Properties;

public class MailHelper extends BaseHelper {

    private ConfigurationDatabaseHelper configDatabaseHelper;
    public MailHelper() {
        log = getLogger(this.getClass());
        configDatabaseHelper = new ConfigurationDatabaseHelper();
    }


    public void sendMail(String recipient, String subject, String content) {
        final String methodName = "sendMail";
        start(methodName);

        HashMap<String, String> mailConfig = configDatabaseHelper.getMailConfiguration();

        final String username = mailConfig.get(ConstantHelper.MAIL_USERNAME);
        final String password = EncryptionManager.getInstance().decrypt(mailConfig.get(ConstantHelper.MAIL_PASSWORD));

        Properties prop = new Properties();
        prop.put("mail.smtp.host", mailConfig.get(ConstantHelper.MAIL_HOST_NAME));
        prop.put("mail.smtp.port", mailConfig.get(ConstantHelper.MAIL_SMTP_PORT));
        prop.put("mail.smtp.auth", mailConfig.get(ConstantHelper.MAIL_SMTP_AUTH));
        prop.put("mail.smtp.starttls.enable", mailConfig.get(ConstantHelper.MAIL_SMTP_TLS)); //TLS
        if (Boolean.valueOf(mailConfig.get(ConstantHelper.MAIL_SMTP_SSL))) {
            prop.put("mail.smtp.socketFactory.port", mailConfig.get(ConstantHelper.MAIL_SMTP_PORT));
            prop.put("mail.smtp.socketFactory.class", mailConfig.get(ConstantHelper.MAIL_SMTP_SOCKETFACTORY_CLASS)); //TLS
        }

        Session session = Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(mailConfig.get(ConstantHelper.MAIL_SERVER_FROM_ADDRESS)));
            message.setRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse(recipient)
            );
            message.setSubject(subject);
            message.setText(content);

            Transport.send(message);
            log.debug(methodName, "Send mail to " + recipient + " successfully");

        } catch (MessagingException e) {
            log.error(methodName, e);
        }
    }
}
