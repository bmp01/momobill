package id.io.momobill.helper;

import id.io.momobill.util.log.AppLogger;

public class BaseHelper {
    protected AppLogger log;

    public BaseHelper() {

    }

    protected <T> AppLogger getLogger(Class<T> clazz) {
        return new AppLogger(clazz);
    }

    protected void start(String methodName) {
        log.debug(methodName, "Start");
    }

    protected void completed(String methodName) {
        log.debug(methodName, "Completed");
    }
}
