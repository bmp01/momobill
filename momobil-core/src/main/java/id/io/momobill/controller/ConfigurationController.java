package id.io.momobill.controller;

import id.io.momobill.util.database.ConfigurationDatabaseHelper;

import java.util.HashMap;

public class ConfigurationController extends BaseController {

    private ConfigurationDatabaseHelper configDbHelper;

    public ConfigurationController() {
        log = getLogger(this.getClass());
        configDbHelper = new ConfigurationDatabaseHelper();
    }

    public HashMap<String, String> getConfigurations() {
        final String methodName = "getConfigurations";
        start(methodName);
        HashMap<String, String> configMap = configDbHelper.getConfiguration();
        completed(methodName);
        return configMap;
    }

    public HashMap<String, String> getOtpConfiguration() {
        final String methodName = "getOtpConfiguration";
        start(methodName);
        HashMap<String, String> configMap = configDbHelper.getOtpConfiguration();
        completed(methodName);
        return configMap;
    }

    public HashMap<String, String> getMailConfiguration() {
        final String methodName = "getMailConfiguration";
        start(methodName);
        HashMap<String, String> configMap = configDbHelper.getMailConfiguration();
        completed(methodName);
        return configMap;
    }
}
