package id.io.momobill.controller;

import id.io.momobill.helper.ConvertionHelper;
import id.io.momobill.helper.MailHelper;
import id.io.momobill.helper.OTPGeneratorHelper;
import id.io.momobill.model.Otp;
import id.io.momobill.util.constant.ConstantHelper;
import id.io.momobill.util.database.ConfigurationDatabaseHelper;
import id.io.momobill.util.database.OtpDatabaseHelper;
import id.io.momobill.util.database.UsersDatabaseHelper;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class OtpController extends BaseController {

    private UsersDatabaseHelper userDbHelper;
    private OtpDatabaseHelper otpDbHelper;
    private ConfigurationDatabaseHelper configDbHelper;

    @Inject
    private ExecutorService executor;

    public OtpController() {
        log = getLogger(this.getClass());
        userDbHelper = new UsersDatabaseHelper();
        otpDbHelper = new OtpDatabaseHelper();
        configDbHelper = new ConfigurationDatabaseHelper();
        executor = Executors.newFixedThreadPool(10);
    }

    public boolean sendOtp(String email) {
        String methodName = "sendOtp";
        start(methodName);
        boolean result = false;
        HashMap<String, String> otpConfig = configDbHelper.getOtpConfiguration();

        String userId = userDbHelper.getUserId(email);
        otpDbHelper.deleteOtpToken(userId);

        String token = OTPGeneratorHelper.generateOtp(Integer.parseInt(otpConfig.get(ConstantHelper.OTP_LENGTH)));
        Otp otp = new Otp();
        otp.setUserId(userId);
        otp.setToken(token);
        if (otpDbHelper.saveOtpToken(otp)) {

            String subject = otpConfig.get(ConstantHelper.OTP_MAIL_SUBJECT);
            String template = otpConfig.get(ConstantHelper.OTP_MAIL_CONTENT);
            String validity = ConvertionHelper.convertSecondToMinute(otpConfig.get(ConstantHelper.OTP_VALIDITY));
            String content = generateOTPContent(template, token, validity);

            log.debug(methodName, email + " : " + token);
            MailHelper mailHelper = new MailHelper();
            executor.execute(() ->
                    mailHelper.sendMail(email, subject, content)
            );
            executor.shutdown();
            result = true;
        }


        completed(methodName);
        return result;
    }

    public boolean validateOtp(Otp otp) {
        String methodName = "validateOtp";
        start(methodName);
        boolean result = false;
        HashMap<String, String> otpConfig = configDbHelper.getOtpConfiguration();

        String userId = userDbHelper.getUserId(otp.getUserId());
        String token = otp.getToken();
        int validity = Integer.parseInt(ConvertionHelper.convertSecondToMinute(otpConfig.get(ConstantHelper.OTP_VALIDITY)));

        if (otpDbHelper.validateOtp(userId, token, validity)) {
            otpDbHelper.deleteOtpToken(userId);
            userDbHelper.activateUser(userId);
            result = true;
        }
        completed(methodName);
        return result;
    }

    public boolean validateOtp(String token) {
        String methodName = "validateOtp";
        start(methodName);
        boolean result = false;
        if (otpDbHelper.validateOtp(token)) {
            result = true;
        }
        completed(methodName);
        return result;
    }

    public boolean validateUser(String email) {
        String methodName = "validateOtp";
        start(methodName);
        boolean result = false;
        if (userDbHelper.validateEmail(email)) {
            result = true;
        }
        completed(methodName);
        return result;
    }

}
