package id.io.momobill.controller;

import id.io.momobill.helper.ConvertionHelper;
import id.io.momobill.helper.MailHelper;
import id.io.momobill.helper.OTPGeneratorHelper;
import id.io.momobill.manager.EncryptionManager;
import id.io.momobill.model.Authentication;
import id.io.momobill.model.Otp;
import id.io.momobill.model.Registration;
import id.io.momobill.model.Users;
import id.io.momobill.util.constant.ConstantHelper;
import id.io.momobill.util.database.AuthenticationDatabaseHelper;
import id.io.momobill.util.database.ConfigurationDatabaseHelper;
import id.io.momobill.util.database.OtpDatabaseHelper;
import id.io.momobill.util.database.UsersDatabaseHelper;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MobileController extends BaseController {

    private AuthenticationDatabaseHelper authDbHelper;
    private UsersDatabaseHelper userDbHelper;
    private OtpDatabaseHelper otpDbHelper;
    private ConfigurationDatabaseHelper configDbHelper;

    @Inject
    private ExecutorService executor;

    public MobileController() {
        log = getLogger(this.getClass());
        authDbHelper = new AuthenticationDatabaseHelper();
        userDbHelper = new UsersDatabaseHelper();
        otpDbHelper = new OtpDatabaseHelper();
        configDbHelper = new ConfigurationDatabaseHelper();
        executor = Executors.newFixedThreadPool(10);
    }

    public boolean authenticate(Authentication auth) {
        final String methodName = "authenticate";
        start(methodName);

        boolean result = false;
        if (authDbHelper.authenticate(auth)) {
            updateLastLoginTime(auth.getUserId());
            result = true;
        }
        completed(methodName);
        return result;
    }

    public boolean register(Registration request) {
        String methodName = "register";
        start(methodName);
        boolean result = false;

        Users user = new Users();
        user.setUserId(request.getUserId());
        user.setUserName(request.getEmail());
        user.setFullName(request.getName());
        user.setEmail(request.getEmail());
        user.setPhone(request.getPhone());

        Authentication auth = new Authentication();
        auth.setUserId(request.getUserId());

        auth.setPassword(EncryptionManager.getInstance().encrypt(request.getPassword()));
        if (userDbHelper.addUser(user) && authDbHelper.createAuthentication(auth)) {
            HashMap<String, String> otpConfig = configDbHelper.getOtpConfiguration();

            String token = OTPGeneratorHelper.generateOtp(Integer.parseInt(otpConfig.get(ConstantHelper.OTP_LENGTH)));
            Otp otp = new Otp();
            otp.setUserId(request.getUserId());
            otp.setToken(token);
            if (otpDbHelper.saveOtpToken(otp)) {

                String subject = otpConfig.get(ConstantHelper.OTP_MAIL_SUBJECT);
                String template = otpConfig.get(ConstantHelper.OTP_MAIL_CONTENT);
                String validity = ConvertionHelper.convertSecondToMinute(otpConfig.get(ConstantHelper.OTP_VALIDITY));
                String content = generateOTPContent(template, token, validity);

                log.debug(methodName, user.getEmail() + " : " + token);
                MailHelper mailHelper = new MailHelper();
                executor.execute(() ->
                        mailHelper.sendMail(user.getEmail(), subject, content)
                );
                executor.shutdown();
                result = true;
            }
        }

        completed(methodName);
        return result;
    }

    public Users getUser(String userId) {
        final String methodName = "getUser";
        start(methodName);
        Users result = userDbHelper.getUser(userId);
        completed(methodName);
        return result;
    }

    public String getUserId(String email) {
        final String methodName = "getUserId";
        start(methodName);
        String result = null;
        String userId = userDbHelper.getUserId(email);
        if (!userId.isEmpty()) {
            result = userId;
        }
        completed(methodName);
        return result;
    }

    public boolean validateEmail(String email) {
        String methodName = "validateEmail";
        start(methodName);
        boolean result = false;

        if (userDbHelper.validateEmail(email)) {
            result = true;
        }

        completed(methodName);
        return result;
    }

    private boolean updateLastLoginTime(String userId) {
        final String methodName = "updateLastLoginTime";
        start(methodName);
        boolean result = false;
        Authentication authentication = new Authentication();
        authentication.setUserId(userId);

        if (authDbHelper.updateUserLastLoginTimestamp(authentication)) {
            result = true;
        }
        completed(methodName);
        return result;
    }

}
