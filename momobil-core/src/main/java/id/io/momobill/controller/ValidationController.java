package id.io.momobill.controller;

import id.io.momobill.util.database.UsersDatabaseHelper;

public class ValidationController extends BaseController {

    private UsersDatabaseHelper userDbHelper;

    public ValidationController() {
        log = getLogger(this.getClass());
        userDbHelper = new UsersDatabaseHelper();
    }

    public boolean validateEmail(String email) {
        String methodName = "validateEmail";
        start(methodName);
        boolean result = false;

        if (userDbHelper.validateEmail(email)) {
            result = true;
        }

        completed(methodName);
        return result;
    }
}
