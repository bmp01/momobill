package id.io.momobill.controller;

import id.io.momobill.model.Users;
import id.io.momobill.model.UsersOLD;
import id.io.momobill.util.database.UsersDatabaseHelper;
import id.io.momobill.util.database.refactor.UsersAddressDatabaseHelper;

import java.util.ArrayList;
import java.util.List;

public class UsersController extends BaseController {

    private UsersDatabaseHelper usersDatabaseHelper;
    private UsersAddressDatabaseHelper usersAddressDatabaseHelper;

    public UsersController() {
        log = getLogger(this.getClass());
        usersDatabaseHelper = new UsersDatabaseHelper();
        usersAddressDatabaseHelper = new UsersAddressDatabaseHelper();
    }

    public Users getUsers(String userId) {
        final String methodName = "getUsers";
        start(methodName);
        Users user = usersDatabaseHelper.getUser(userId);
        completed(methodName);
        return user;
    }

    public List<Users> getUsers() {
        final String methodName = "getUsers";
        start(methodName);

        List<Users> usersDbList = usersDatabaseHelper.getUsers();

        List<Users> usersList = new ArrayList<>();
        Users users = new Users();
        for (Users usersDb : usersDbList) {
            users.setUserId(usersDb.getUserId());
            users.setUserName(usersDb.getUserName());
            users.setFullName(usersDb.getFullName());
            users.setEmail(usersDb.getEmail());
            users.setPhone(usersDb.getPhone());
            users.setPicture(usersDb.getPicture());
            users.setCreateDt(usersDb.getCreateDt());

            usersList.add(users);
        }

        completed(methodName);
        return usersList;
    }

    public boolean createUser(Users user) {
        String methodName = "createUser";
        start(methodName);
        boolean result = false;
        if (usersDatabaseHelper.addUser(user)) {
            result = true;
        }
        completed(methodName);
        return result;
    }

    private Users convertRequestToModel(UsersOLD user) {
        Users dbUsers = new Users();

        return dbUsers;
    }

    //unchecked
    /*public JSONObject getUser(String userId) {
        final String methodName = "getUser";
        start(methodName);

        Users user = usersDatabaseHelper.getUser(userId);
        JSONObject response = new JSONObject();
        if (user != null) {
            JSONObject userObj = new JSONObject();
            userObj.put(ConstantHelper.KEY_USER_USERID, user.getUserid());
            userObj.put(ConstantHelper.KEY_USER_USERNAME, user.getUsername());
            userObj.put(ConstantHelper.KEY_USER_FULLNAME, user.getFullname());
            userObj.put(ConstantHelper.KEY_USER_EMAIL, user.getEmail());

            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_OK);
            response.put(ConstantHelper.HTTP_RESPONSE, userObj.toString());
        } else {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_BAD_REQUEST);
            response.put(ConstantHelper.HTTP_REASON, ConstantHelper.ERROR_REASON_USER_NOT_FOUND);
            response.put(ConstantHelper.HTTP_MESSAGE, ConstantHelper.ERROR_MESSAGE_USER_NOT_FOUND);
        }

        completed(methodName);
        return response;
    }


    public JSONObject getDetailedUser(String userId) {
        final String methodName = "getDetailedUser";
        start(methodName);
        JSONObject response = new JSONObject();

        Users user = usersDatabaseHelper.getUser(userId);

        if (user != null) {
            JSONObject userObj = new JSONObject();
            userObj.put(ConstantHelper.KEY_USER_USERID, user.getUserid());
            userObj.put(ConstantHelper.KEY_USER_USERNAME, user.getUsername());
            userObj.put(ConstantHelper.KEY_USER_FULLNAME, user.getFullname());
            userObj.put(ConstantHelper.KEY_USER_EMAIL, user.getEmail());
            userObj.put(ConstantHelper.KEY_USER_PHONE, user.getPhone());
            userObj.put(ConstantHelper.KEY_USER_AVATAR, user.getAvatar());
            userObj.put(ConstantHelper.KEY_USER_CREATEDT, user.getCreatedt());

            List<UsersAddress> addressList = usersAddressDatabaseHelper.getAddresses(userId);
            JSONArray addressArray = new JSONArray();
            for (UsersAddress usersAddress : addressList) {
                JSONObject addressObj = new JSONObject();
                addressObj.put(ConstantHelper.KEY_ADDRESS_TAGS, usersAddress.getTags());
                addressObj.put(ConstantHelper.KEY_ADDRESS_ADDRESS, usersAddress.getAddress());
                addressObj.put(ConstantHelper.KEY_ADDRESS_LATITUDE, usersAddress.getLatitude());
                addressObj.put(ConstantHelper.KEY_ADDRESS_LONGITUDE, usersAddress.getLongitude());
                addressArray.put(addressObj);
            }

            userObj.put(ConstantHelper.KEY_USER_ADDRESSES, addressArray);

            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_OK);
            response.put(ConstantHelper.HTTP_RESPONSE, userObj.toString());
        } else {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_BAD_REQUEST);
            response.put(ConstantHelper.HTTP_REASON, ConstantHelper.ERROR_REASON_USER_NOT_FOUND);
            response.put(ConstantHelper.HTTP_MESSAGE, ConstantHelper.ERROR_MESSAGE_USER_NOT_FOUND);
        }
        completed(methodName);

        return response;
    }

    public JSONObject getAddresses(String userId) {
        final String methodName = "getAddresses";
        start(methodName);
        JSONObject response = new JSONObject();

        List<UsersAddress> addressList = usersAddressDatabaseHelper.getAddresses(userId);

        JSONArray addressArray = new JSONArray();
        for (UsersAddress usersAddress : addressList) {
            JSONObject addressObj = new JSONObject();
            addressObj.put(ConstantHelper.KEY_ADDRESS_TAGS, usersAddress.getTags());
            addressObj.put(ConstantHelper.KEY_ADDRESS_ADDRESS, usersAddress.getAddress());
            addressObj.put(ConstantHelper.KEY_ADDRESS_LATITUDE, usersAddress.getLatitude());
            addressObj.put(ConstantHelper.KEY_ADDRESS_LONGITUDE, usersAddress.getLongitude());
            addressArray.put(addressObj);
        }
        response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_OK);
        response.put(ConstantHelper.HTTP_RESPONSE, addressArray);
        completed(methodName);
        return response;
    }

    public boolean validateUsername(String username) {
        final String methodName = "validateUsername";
        start(methodName);
        boolean result = false;

        if (!usersDatabaseHelper.validateUsername(username)) {
            result = true;
        }
        completed(methodName);
        return result;
    }

    public boolean updateUser(JSONObject json) {
        final String methodName = "updateUser";
        start(methodName);
        boolean result = false;

        Users user = new Users();
        user.setUserid(json.getString(ConstantHelper.KEY_USER_USERID));
        user.setUsername(json.getString(ConstantHelper.KEY_USER_USERNAME));
        user.setFullname(json.getString(ConstantHelper.KEY_USER_FULLNAME));
        user.setEmail(json.getString(ConstantHelper.KEY_USER_EMAIL));
        user.setPhone(json.getString(ConstantHelper.KEY_USER_PHONE));

        if (usersDatabaseHelper.updateUser(user)) {
            result = true;
        }
        completed(methodName);
        return result;
    }

    public boolean updateAddress(JSONObject json) {
        final String methodName = "updateAddress";
        start(methodName);
        boolean result = false;

        Users user = new Users();
        user.setUserid(json.getString(ConstantHelper.KEY_USER_USERID));
        user.setFullname(json.getString(ConstantHelper.KEY_USER_FULLNAME));
        user.setEmail(json.getString(ConstantHelper.KEY_USER_EMAIL));
        user.setPhone(json.getString(ConstantHelper.KEY_USER_PHONE));
        user.setAvatar(json.getString(ConstantHelper.KEY_USER_AVATAR));

        if (usersDatabaseHelper.updateUser(user)) {
            result = true;
        }
        completed(methodName);
        return result;
    }*/

}
