package id.io.momobill.controller;

import id.io.momobill.util.log.AppLogger;

import java.util.concurrent.ExecutorService;

public class BaseController {

    ExecutorService executor;

    protected AppLogger log;

    protected AppLogger getLogger(Class<?> clazz) {
        return new AppLogger(clazz);
    }

    protected void start(String methodName) {
        log.debug(methodName, "start");
    }

    protected void completed(String methodName) {
        log.debug(methodName, "completed");
    }

    public String generateOTPContent(String template, String otp, String validity) {
        String result = template.replaceAll("&otpToken&", otp).replaceAll("&otpValidity&", validity);
        return result;
    }
}
