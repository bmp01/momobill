package id.io.momobill.controller;

import id.io.momobill.model.Authentication;
import id.io.momobill.util.database.AuthenticationDatabaseHelper;
import id.io.momobill.util.database.UsersDatabaseHelper;

public class AuthenticationController extends BaseController {

    private UsersDatabaseHelper usersDatabaseHelper;
    private AuthenticationDatabaseHelper authenticationDatabaseHelper;

    public AuthenticationController() {
        log = getLogger(this.getClass());
        usersDatabaseHelper = new UsersDatabaseHelper();
        authenticationDatabaseHelper = new AuthenticationDatabaseHelper();
    }

    public boolean authenticate(Authentication auth) {
        final String methodName = "authenticate";
        start(methodName);

        boolean result = false;
        String userId = getUserId(auth.getUserId());
        if (!userId.isEmpty()) {
            Authentication dbAuthentication = new Authentication();
            dbAuthentication.setUserId(userId);
            dbAuthentication.setPassword(auth.getPassword());
            if (authenticationDatabaseHelper.authenticate(dbAuthentication)) {
                updateLastLoginTime(userId);
                result = true;
            }
        }
        completed(methodName);
        return result;
    }

    public boolean registration(Authentication dbAuthentication) {
        final String methodName = "registration";
        start(methodName);

        boolean result = false;

        if (authenticationDatabaseHelper.createAuthentication(dbAuthentication)) {
            updateLastLoginTime(dbAuthentication.getUserId());
            result = true;
        }

        completed(methodName);
        return result;
    }

    public String getUserId(String email) {
        final String methodName = "getUserId";
        start(methodName);
        String result = null;
        String userId = usersDatabaseHelper.getUserId(email);
        if (!userId.isEmpty()) {
            completed(methodName);
            result = userId;
        }
        completed(methodName);
        return result;
    }

    private boolean updateLastLoginTime(String userId) {
        final String methodName = "updateLastLoginTime";
        start(methodName);
        boolean result = false;
        Authentication authentication = new Authentication();
        authentication.setUserId(userId);

        if (authenticationDatabaseHelper.updateUserLastLoginTimestamp(authentication)) {
            completed(methodName);
            result = true;
        }
        completed(methodName);
        return result;
    }
}
