package id.io.momobill.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UsersOLD {

    @JsonProperty("user-id")
    private String userId;
    @JsonProperty("user-name")
    private String userName;
    @JsonProperty("full-name")
    private String fullName;
    @JsonProperty("email")
    private String email;
    @JsonProperty("phone")
    private String phone;
    @JsonProperty("img")
    private String pict;
    @JsonProperty("create-dt")
    private String createDt;


    public UsersOLD(String userId) {
        this.userId = userId;
    }

    public UsersOLD(String userId, String userName, String fullName, String email, String phone, String pict, String createDt) {
        this.userId = userId;
        this.userName = userName;
        this.fullName = fullName;
        this.email = email;
        this.phone = phone;
        this.pict = pict;
        this.createDt = createDt;
    }

    public UsersOLD() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPict() {
        return pict;
    }

    public void setPict(String pict) {
        this.pict = pict;
    }

    public String getCreateDt() {
        return createDt;
    }

    public void setCreateDt(String createDt) {
        this.createDt = createDt;
    }
}
